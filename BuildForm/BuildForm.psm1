﻿function Get-BuildForms
{
	<#
	.SYNOPSIS
	Retrieves the build forms and creates objects from each form
	.DESCRIPTION
	
	.PARAMETER
	.EXAMPLE
	.NOTES 
	Name:
	Author:
	Date:
	Email:
	Keywords: 
	#Requires -Version 4.0
	#>
	[CmdLetbinding()]
	param
	(
	
	)
	BEGIN
	{
		$FileNames = $null
		$Global:Forms = $null
		$FormHeaders = $null
		#Configure a default display set
		Update-TypeData -TypeName BuildForm -DefaultDisplayPropertySet TicketNumber, BuildType, ComputerName, OldName, ServiceTag, AssetTag, OSInstallDate, WarrantyDate, Technician -force
	} #Begin
	PROCESS
	{
		# Retrieves Ticket Number from each file in directory
		$FormHeaders = Get-Content C:\BuildFiles\*.txt  -Head 1
		$FileNames = @()
		# Loop to build object from header and correct file
		foreach ($Header in $FormHeaders)
		{
			# Splits the current $Header
			$Header = $Header.split(":")
			# Adds $Header to a $FileNames array
			$FileNames += $Header[1]
			$Global:Forms = @()
			foreach ($Name in $FileNames)
			{
				# Assigns array the contents of a build form
				$File = (Get-Content C:\BuildFiles\$Name.txt).split(":")
				# Creates object from the split data
				$BuildForm = New-Object -TypeName PSObject -Property @{
					"TicketNumber" = $File[1]
					"BuildType" = $File[3]
					"ComputerName" = $File[5]
					"OldName" = $FIle[7]
					"ServiceTag" = $File[9]
					"AssetTag" = $File[11]
					"OSInstallDate" = Get-Date -Date $File[13]
					"WarrantyDate" = Get-Date -Date $File[15]
					"Technician" = $File[17]
				}
				# After object is created we assign it a Type, that way it can have a default disply type of BuildForm
				#Give this object a unique typename
				$BuildForm.PSTypeNames.Insert(0, 'BuildForm')
				# Adds $BuildForm to object array $Forms
				$Global:Forms += $BuildForm
			}
		}
	}
	End { } #End
}
