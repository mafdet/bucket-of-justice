﻿function Get-BuildForms
{
	<#
	.SYNOPSIS
	Retrieves the build forms and creates objects from each form
	.DESCRIPTION
	
	.PARAMETER
	.EXAMPLE
	.NOTES 
	Name:
	Author:
	Date:
	Email:
	Keywords: 
	#Requires -Version 4.0
	#>
	[CmdLetbinding()]
	param
	(
	
	)
	BEGIN
	{
		$FileNames = $null
		$Global:Forms = $null
		$FormHeaders = $null
	} #Begin
	PROCESS
	{
		# Retrieves Ticket Number from each file in directory
		$FormHeaders = Get-Content C:\BuildFiles\*.txt -Head 1
		$FileNames = @()
		# Loop to build object from header and correct file
		foreach ($Header in $FormHeaders)
		{
			# Splits the current $Header
			$Header = $Header.split(" ")
			$Global:Forms = @()
			# Adds $Header to a $FileNames array
			$FileNames += $Header[2]
			foreach ($Name in $FileNames)
			{
				# Assigns array the contents of a build form
				$File = (Get-Content C:\BuildFiles\$Name.txt).split(":")
				# Splits each section of the build form into another mini array
				# Creates object from the split data
				$BuildForm = New-Object -TypeName PSObject -Property @{
					"TicketNumber" = $File[1]
					"BuildType" = $File[3]
					"ComputerName" = $File[5]
					"OldName" = $FIle[7]
					"ServiceTag" = $File[9]
					"AssetTag" = $File[11]
					"OSInstallDate" = $File[13]
					"WarrantyDate" = $File[15]
					"Technician" = $File[17]
				}
				# Adds $BuildForm to object array $Forms
				$Global:Forms += $BuildForm
			}
		}
	}
	End { } #End
}
