﻿function Get-BancorpOU
{
<#
	.Synopsis
		Displays The Bancorp's site locations based on 3 criteria: Laptops, Workstations, All
	
	.Description
		The Get-BancorpOU displays site location OU's based on hardwaretype. Desktop SUpport
		only needs to worry about Laptops and Workstations.
	
	.PARAMETER HardwareType
		A description of the HardwareType parameter.
	
	.EXAMPLE
		PS C:\> Get-BancorpOU -HardwareType $value1
	
	.NOTES
		Additional information about the function.
#>
	
	switch ($HardwareType)
	{
		Laptops
		{
			return $Script:SiteOU = Get-ADOrganizationalUnit -Filter 'name -like "*"' -SearchBase "OU=$HardwareType, OU=hardware,OU=bancorp,DC=thebancorp,DC=local" -SearchScope OneLevel | Select-Object -expand Name
		}
		Workstations
		{
			return $Script:SiteOU = Get-ADOrganizationalUnit -Filter 'name -like "*"' -SearchBase "OU=$HardwareType, OU=hardware,OU=bancorp,DC=thebancorp,DC=local" -SearchScope OneLevel | Select-Object -expand Name
		}
		All
		{
			$LaptopOU = Get-ADOrganizationalUnit -Filter 'name -like "*"' -SearchBase "OU=Laptops, OU=hardware,OU=bancorp,DC=thebancorp,DC=local" -SearchScope OneLevel | Select-Object -expand Name
			$WorkstationOU = Get-ADOrganizationalUnit -Filter 'name -like "*"' -SearchBase "OU=Workstations, OU=hardware,OU=bancorp,DC=thebancorp,DC=local" -SearchScope OneLevel | Select-Object -expand Name
			return $Script:SiteOU = $WorkstationOU + $LaptopOU | select -Unique | sort
		}
	}
}
Get-BancorpOU Laptops