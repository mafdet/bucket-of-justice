﻿Import-Module Bancorp -force -DisableNameChecking
$ComputerName = "WS-TA-LA01631"
$MacAddress = "5CF9DDDCE873"
Write-Warning "Beginning Deployment process don't turn off equipment or disrupt this script."
Write-Verbose "Creating new computer in DSM..." -Verbose
New-DSMComputer $ComputerName $MacAddress -Verbose
Write-Verbose "Adding computer to specified OS package..." -Verbose
Add-OSPackage $ComputerName Win_7_64 -Verbose
Write-Verbose "Adding computer to the Core Apps group..." -Verbose
Add-CoreApps $ComputerName -Verbose
Write-Verbose "Adding the computer to correct OU in Active Directory..." -Verbose
New-ComputerProperOU $ComputerName
Write-Verbose "Deploying..." -Verbose
$DSMComputer = Get-EmdbComputer -Filter "(&(name=$ComputerName))" -recurse
Do
{
    $DSMComputer = Get-EmdbComputer -Filter "(&(name=$ComputerName))" -recurse
    $DSMComputer.LastCalculatedCompliance
} Until ($DSMComputer.LastCalculatedCompliance -eq "Compliant")
Write-Verbose "Compliance reached in DSM..." -Verbose
