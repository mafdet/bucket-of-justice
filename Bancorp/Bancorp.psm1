﻿Import-Module ActiveDirectory -Force -ErrorAction SilentlyContinue
Import-Module PSX7 -Force -ErrorAction SilentlyContinue -DisableNameChecking 

# Adds DSM computer object to Core Apps group
Function Add-CoreApps
{
	<#
	.SYNOPSIS
        Adds DSM computer object to the Core Apps group
	.DESCRIPTION
        Adds DSM computer object to the core apps group. Core Apps belong to The Bancorp's base build.
	.PARAMETER ComputerName

	.EXAMPLE
        Add-CoreApps WS-DE-5290
        Adds WS-DE-5290 computer object to the Core Apps group in DSM.
	.NOTES 
	    Name: Add-CoreApps
	    Author: Scott Goyette
	    Date: 03/21/2016
	    Email: sgoyette@thebancorp.com, scottcgoyette@yahoo.com
	    Keywords: 
	#>
	[CmdLetbinding(SupportsShouldProcess=$True)]
	param
	(
        [Parameter (Position=0, Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$True)]
		[String[]]$ComputerName
	)
	BEGIN
	{
        # Section moves into the correct PSDrive space based on hostname executing command
        $ExecutionHost = $ENV:COMPUTERNAME
        Write-Verbose "Executing command from $ExecutionHost..."
        switch($ExecutionHost)
        {
            "PPPH-DEPLOY-1"
            {
                Mount-DSM Philadelphia
                Write-verbose "Philadelphia: Mounted accessing drive..."
                $Server ="Philadelphia:\RootDSE\Managed Users & Computers\Software"
            }
            "PPSF-DEPLOY-2"
            {
                Mount-DSM Sioux
                Write-verbose "Sioux: Mounted accessing drive..."
                $Server ="Sioux:\RootDSE\Managed Users & Computers\Software"    
            }
            "VPBG2-DSM-1"
            {
                Mount-DSM Bulgaria
                Write-verbose "Bulgaria: Mounted accessing drive..."
                $Server ="Bulgaria:\RootDSE\Managed Users & Computers\Software"    
            }
            "VPWI-DSM-1"
            {
                Mount-DSM Delaware
                Write-verbose "Delaware: Mounted accessing drive..."
                $Server ="Delaware:\RootDSE\Managed Users & Computers\Software"
            }
        }
    }#Begin
	PROCESS
	{
	    # If DSM computer object exists
        Write-Verbose "Verifying computer exists on DSM server..."
        If (Get-EmdbComputer "$ComputerName" -recurse)
        {
            Write-Verbose "Computer exists, adding to Core Apps group..."
            $CoreApps = Get-EmdbGroup "$Server\!Group- Core Applications" -SchemaTag "Group"
            $DSMComputer = Get-EmdbComputer -Filter "(&(name=$ComputerName))" -recurse
            Add-EmdbGroupMember -Group $CoreApps -Member $DSMComputer
            Write-Verbose "$ComputerName successfully added to the Core Apps group."
        }
        Else
        {
            Write-Verbose "Computer $ComputerName does not exist, try creating it before assigning packages." -Verbose
        }	
	}
	End { } #End
}
# Adds DSM computer object to OS package group
Function Add-OSPackage
{
	<#
	.SYNOPSIS
        Assigns an OS Package to a computer object. 
	.DESCRIPTION
        Assigns an OS Package to a computer object on the DSM server. The entries to this script will need to be
        modified if adding more OS packages or reducing OS packages.
	.PARAMETER ComputerName

    .PARAMETER OSPackage

	.EXAMPLE
        Add-OSPackage WS-DE-5290 Win_7_64
        Adds computer WS-DE-5290 to the Windows 7 (x64) Pro DSM OS Deployment group
    .EXAMPLE
        Add-OSPackage WS-DE-5290 Win_7_32
        Adds computer WS-DE-5290 to the Windows 7 (x86) Pro DSM OS Deployment group
    .EXAMPLE
        Add-OSPackage WS-DE-5290 Win_8.1_64
        Adds computer WS-DE-5290 to the Windows 8.1 (x64) Pro DSM OS Deployment group
	.NOTES 
	    Name: Add-OSPackage
	    Author: Scott Goyette
	    Date: 03/21/2016
	    Email: sgoyette@thebancorp.com, scottcgoyette@yahoo.com
	    Keywords: DSM, Add, Operating System, OS Type
	#>
	[CmdLetbinding(SupportsShouldProcess=$True)]
	param
	(
        [Parameter (Position=0, Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$True)]
		[String[]]$ComputerName,
        [Parameter (Position=1, Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$True)]
        [ValidateSet("Windows 10 (x64) Pro", "Windows 7 (x64) Pro", "Windows 7 (x86) Pro", "Windows 8.1 (x64) Pro")]
        [String[]]$OSPackage
	)
	BEGIN
	{
        # Section moves into the correct PSDrive space based on hostname executing command
        $ExecutionHost = $ENV:COMPUTERNAME
        Write-Verbose "Executing command from $ExecutionHost..."
        Split-ComputerName $ComputerName
        switch($ExecutionHost)
        {
            "PPPH-DEPLOY-1"
            {
                Mount-DSM Philadelphia
                Write-verbose "Philadelphia: Mounted accessing drive..."
                $Server ="Philadelphia:\RootDSE\Managed Users & Computers\OS Deployment"
            }
            "PPSF-DEPLOY-2"
            {
                Mount-DSM Sioux
                Write-verbose "Sioux: Mounted accessing drive..."
                $Server ="Sioux:\RootDSE\Managed Users & Computers\OS Deployment"    
            }
            "VPBG2-DSM-1"
            {
                Mount-DSM Bulgaria
                Write-verbose "Bulgaria: Mounted accessing drive..."
                $Server ="Bulgaria:\RootDSE\Managed Users & Computers\OS Deployment"    
            }
            "VPWI-DSM-1"
            {
                Mount-DSM Delaware
                Write-verbose "Delaware: Mounted accessing drive..."
                $Server ="Delaware:\RootDSE\Managed Users & Computers\OS Deployment"
            }
        }
    }#Begin
	PROCESS
	{
        # If DSM computer object exists
        Write-Verbose "Verifying computer exists on DSM server..."
	    If (Get-EmdbComputer "$ComputerName" -recurse)
        {
            # See which OS package to install
            Write-Verbose "Computer exists, using $OSPackage for OS Package choice..."
            Switch($OSPackage)
            {
                # Currently we have 3 OS Packages, shorthanded them for brevity at the commandline.
                # Windows 7 64 Bit
                "Windows 7 (x64) Pro" 
                {
                    Write-Verbose "Adding OS package: Windows 7 64 Bit..." 
                    $OS = Get-EmdbGroup "$Server\Windows 7 (x64) Pro" -SchemaTag "Group"
                    $DSMComputer = Get-EmdbComputer -Filter "(&(name=$ComputerName))" -recurse
                    Add-EmdbGroupMember -Group $OS -Member $DSMComputer
                    Write-Verbose "Package added successfuly." 
                }
                # Windows 7 32 Bit
                "Windows 7 (x86) Pro"
                {
                    Write-Verbose "Adding OS package: Windows 7 32 Bit..." 
                    $OS = Get-EmdbGroup "$Server\Windows 7 (x86) Pro" -SchemaTag "Group"
                    $DSMComputer = Get-EmdbComputer -Filter "(&(name=$ComputerName))" -recurse
                    Add-EmdbGroupMember -Group $OS -Member $DSMComputer
                    Write-Verbose "Package added successfuly."
                }
                # Windows 8.1 64 Bit
                "Windows 8.1 (x64) Pro"
                {
                    Write-Verbose "Adding OS package: Windows 8.1 64 Bit..."
                    $OS = Get-EmdbGroup "$Server\Windows 8.1 (x64) Pro" -SchemaTag "Group"
                    $DSMComputer = Get-EmdbComputer -Filter "(&(name=$ComputerName))" -recurse
                    Add-EmdbGroupMember -Group $OS -Member $DSMComputer
                    Write-Verbose "Package added successfuly."
                }
                # Windows 10 64 Bit
                "Windows 10 (x64) Pro"
                {
                    Write-Verbose "Adding OS package: Windows 10 64 Bit..."
                    $OS = Get-EmdbGroup "$Server\Windows 10 (x64) Pro" -SchemaTag "Group"
                    $DSMComputer = Get-EmdbComputer -Filter "(&(name=$ComputerName))" -recurse
                    Add-EmdbGroupMember -Group $OS -Member $DSMComputer
                    Write-Verbose "Package added successfuly."
                }
            }
        }
        Else
        {
            Write-Verbose "Computer Object: $ComputerName, does not exist. Consider creating first." -Verbose
        }	
	}
	End { } #End
}
# Adds computer to software package group in DSM
Function Add-SoftwarePackage
{
	<#
	.SYNOPSIS
        Adds software package to target machine in DSM
	.DESCRIPTION
        Must be executed from a DSM server node. Adds computer to software package group in DSM. Wrap the SoftwarePackage in double quotes ""
        to ensure spaces are read.
	.PARAMETER ComputerName

    .PARAMETER SoftwarePackage

    .PARAMETER List

	.EXAMPLE
        Add-SoftwarePackage -List
        Will output our current list of software one-offs.
    .EXAMPLE
        Add-SoftwarePackage WS-DE-5290 Adobe
        Will list all packages with the word "Adobe" in them.
    .EXAMPLE
        Add-SoftwarePackage WS-DE-5290 "Adobe Acrobat Standard DC" will add the computer to the Adobe Acrobat Standard DC software group.
	.NOTES 
	    Name: Add-SoftwarePackage
	    Author: Scott Goyette
	    Date: 04/04/2016
	    Email: sgoyette@thebancorp.com, scottcgoyette@yahoo.com
	    Keywords: Software, Package, Software Pa  ckage, Add
	#>
	[CmdLetbinding(SupportsShouldProcess=$True)]
	param
	(
        [Parameter (Position=0, Mandatory=$False,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$True)]
		[String[]]$ComputerName,
        [Parameter (Position=1, Mandatory=$False,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$True)]
        [String[]]$SoftwarePackage,
        
        [Switch]$List
	)
	BEGIN
	{
        # Section moves into the correct PSDrive space based on hostname executing command
        $ExecutionHost = $ENV:COMPUTERNAME
        Write-Verbose "Executing command from $ExecutionHost..."
        switch($ExecutionHost)
        {
            "PPPH-DEPLOY-1"
            {
                Mount-DSM Philadelphia
                Write-verbose "Philadelphia: Mounted accessing drive..."
                $Server ="Philadelphia:\RootDSE\Managed Users & Computers\"
            }
            "PPSF-DEPLOY-2"
            {
                Mount-DSM Sioux
                Write-verbose "Sioux: Mounted accessing drive..."
                $Server ="Sioux:\RootDSE\Managed Users & Computers\"    
            }
            "VPBG2-DSM-1"
            {
                Mount-DSM Bulgaria
                Write-verbose "Bulgaria: Mounted accessing drive..."
                $Server ="Bulgaria:\RootDSE\Managed Users & Computers\"    
            }
            "VPWI-DSM-1"
            {
                Mount-DSM Delaware
                Write-verbose "Delaware: Mounted accessing drive..."
                $Server ="Delaware:\RootDSE\Managed Users & Computers\"
            }
        }
        # Gets a listing of current Software One-offs.
        $SoftwarePackageList = Get-emdbGroup "$Server\Software\Software- One offs\*" -SchemaTag "Group" | sort
        # If the $List switch is set at Cmdlet run, will output a listing of all applications we have for
        # Software one offs.
        If ($List -eq $True)
        {
            Write-Output $SoftwarePackageList | select -ExpandProperty Name
            Break
        }
    }#Begin
	PROCESS
	{
        Set-Location $Server
        # Verifies a Computer name was typed in.
        If ($ComputerName -eq $Null)
        {
            Write-Verbose "Must specify a computer name to add to SoftwarePackage group." -Verbose
            Break
        }
        # Verifies that computer name specified exists in DSM as a computer object.
        Write-Verbose "Verifying computer exists on DSM server..."
        If (Get-EmdbComputer "$ComputerName" -recurse)
        {
            Write-Verbose "Verifying you chose a software package in our software list"
            # Verifies that the $SoftwarePackage variable contains a valid software package.
            If ($SoftwarePackageList.Name -Like $SoftwarePackage)
            {
                # Begins to add the computer to the software package's group.
                Write-Verbose "Computer exists, Adding to $SoftwarePackage group..."
                $DSMComputer = Get-EmdbComputer $ComputerName -recurse
                $SoftPackage = Get-emdbGroup "$Server\Software\Software- One offs\$SoftwarePackage" -SchemaTag "Group"
                Add-EmdbGroupMember -Group $SoftPackage -Member $DSMComputer
                Write-Verbose "Successfully added $ComputerName to $SoftwarePackage group."
            }
            # Verifies that the SoftwarePackage is not equal to $Null or nothing. Must contain value
            # Will output a listing of packages that could be what you are looking for based on value in the $SoftwarePackage variable.
            ElseIf ($SoftwarePackage -ne $Null)
            {
                Write-Verbose "Software package does not exist, did you mean?" -Verbose
                Write-Output $SoftwarePackageList | Where-Object {$_.Name -like "*$SoftwarePackage*"}| Select -Expand Name
            }
            Else
            {
                Write-Verbose "Missing SoftwarePackage argument!" -Verbose
            }
        }
        Else
        {
            Write-Verbose "Computer $ComputerName does not exist, try creating it first before assigning packages." -Verbose
        }	
	}
	End { } #End
}
# Copies a DSM computer objects OS and packages to a new DSM computer object
Function Copy-DSMComputer
{
	<#
	.SYNOPSIS
        Cmdlet allows for cloning of computer objects in DSM, either a fresh computer object or existing object.
	.DESCRIPTION
        
	.PARAMETER ComputerName

	.EXAMPLE

	.NOTES 
	    Name: Copy-DSMComputer
	    Author: Scott Goyette
	    Date: 05/05/2016
	    Email: sgoyette@thebancorp.com, scottcgoyette@yahoo.com
	    Keywords:
	#>
	[CmdLetbinding(SupportsShouldProcess=$True)]
	param
	(
        [Parameter (Position=0, Mandatory=$True,
					ValueFromPipeline=$True,
					ValueFromPipelineByPropertyName=$True)]
		[String[]]$CloneComputer,
        [Parameter (Position=1, Mandatory=$True,
					ValueFromPipeline=$True,
					ValueFromPipelineByPropertyName=$True)]
		[String[]]$NewComputer,
        [Parameter(Position=2, Mandatory=$true, ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)]
        [ValidateScript({
            $pattern = @(
            '^([0-9a-f]{12})$'
            )
            if ($_ -match $pattern) {$true} else {
                    throw "The argument '$_' does not match a valid MAC address format."
            }
        })]
        [string]$MacAddress
	)
	BEGIN
	{
       # Section moves into the correct PSDrive space based on hostname executing command
        $ExecutionHost = $ENV:COMPUTERNAME
        Write-Verbose "Executing command from $ExecutionHost..."
        switch($ExecutionHost)
        {
            "PPPH-DEPLOY-1"
            {
                Mount-DSM Philadelphia
                Write-verbose "Philadelphia: Mounted accessing drive..."
                $Server ="Philadelphia:\RootDSE\Managed Users & Computers\"
            }
            "PPSF-DEPLOY-2"
            {
                Mount-DSM Sioux
                Write-verbose "Sioux: Mounted accessing drive..."
                $Server ="Sioux:\RootDSE\Managed Users & Computers\"    
            }
            "VPBG2-DSM-1"
            {
                Mount-DSM Bulgaria
                Write-verbose "Bulgaria: Mounted accessing drive..."
                $Server ="Bulgaria:\RootDSE\Managed Users & Computers\"    
            }
            "VPWI-DSM-1"
            {
                Mount-DSM Delaware
                Write-verbose "Delaware: Mounted accessing drive..."
                $Server ="Delaware:\RootDSE\Managed Users & Computers\"
            }
        }
        New-DSMComputer $NewComputer $MacAddress
        New-ComputerProperOU $NewComputer
        $Computer = Get-EmdbComputer -Filter "(&(name=$CloneComputer))" -recurse
        $AppList = $Computer.GetStaticGroupMemberships()
    }#Begin
	PROCESS
	{
        ForEach ($App in $AppList.Name)
        {
            Switch -Wildcard ($App)
            {
                "!Group- Core*" { Add-CoreApps $NewComputer }
                "Wind*" { Add-OSPackage $NewComputer $App }
                default { Add-SoftwarePackage $NewComputer "$App" }
   
            }   
        }	
	}
	End { } #End
}
# Dismounts a DSM server PSDrive 
function Dismount-DSM
{
	<#
	.SYNOPSIS
        Dismounts a DSM server PSDrive
	.DESCRIPTION
	    Cmdlet must be run from a DSM server node currently. Dismounts a DSM server PSDrive, will remove the drive from the system the Cmdlet is executed from.
    .PARAMETER Server

	.EXAMPLE
        Dismount-DSM Bulgaria
        Will dismount the Bulgaria DSM server PSDrive from the system
    .EXAMPLE
        Dismount-DSM Delaware
        Will dismount the Delaware DSM server PSDrive from the system
    .EXAMPLE
        Dismount-DSM Philadelphia
        Will dismount the Philadelphia DSM server PSDrive from the system
    .EXAMPLE
        Dismount-DSM Sioux
        Will dismount the Sioux Falls DSM server PSDrive from the system
	.NOTES 
	    Name: Dismount-DSM
	    Author: Scott Goyette
	    Date: 03/14/2016
	    Email: sgoyette@thebancorp.com, scottcgoyette@yahoo.com
	    Keywords: Dismount, DSM
	#>
	[CmdLetbinding(SupportsShouldProcess=$True)]
	param
	(
        [Parameter (Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$true)]
		[ValidateSet("Delaware", "Philadelphia", "Sioux", "Bulgaria")]
        [String[]]$Server
	)
	BEGIN
	{}#Begin
	PROCESS
	{
	    ForEach ($Name in $Server)
        {
            Try
            {
                Write-Verbose "Establishing Connection to $Server DSM server..."
                Switch($Server)
                {
                    # Dismounts the Bulgaria PSDrive.
                    "Bulgaria"
                    {
                        # If server can be reached.
                        If (Test-Connection -computername vpbg2-dsm-1 -count 1 -quiet)
                        {
                            Write-Verbose "Connection established..."
                            # If PSDrive already exists
                            If (Get-PSDrive -Name Bulgaria -ErrorAction SilentlyContinue)
                            {
                                Write-Verbose "Attempting to dismount PSDrive: $Server..."
                                # Dismounts drive
                                Remove-PSDrive Bulgaria
                                Write-Verbose "PSDrive: $Server has been dismounted."
                            }
                            Else
                            {
                                Write-Verbose "$Server does not exists as PSDrive..." -Verbose
                            }
                        }
                        Else
                        {
                            Write-Verbose "Connection could not be established, terminating..." -Verbose
                        }
                    }
                    # Dismounts the Delaware PSDrive.
                    "Delaware"
                    {
                        # If server can be reached.
                        If (Test-Connection -computername vpwi-dsm-1 -count 1 -quiet)
                        {
                            Write-Verbose "Connection established..."
                            # If PSDrive already exists
                            If (Get-PSDrive -Name Delaware -ErrorAction SilentlyContinue)
                            {
                                Write-Verbose "Attempting to dismount PSDrive: $Server..."
                                # Dismounts drive
                                Remove-PSDrive Delaware
                                Write-Verbose "PSDrive: $Server has been dismounted."
                            }
                            Else
                            {
                                Write-Verbose "$Server does not exists as PSDrive..." -Verbose
                            }
                        }
                        Else
                        {
                            Write-Verbose "Connection could not be established, terminating..." -Verbose
                        }
                    }
                    # Dismounts the Philadelphia PSDrive.
                    "Philadelphia"
                    {
                        # If server can be reached.
                        If (Test-Connection -computername ppph-deploy-1 -count 1 -quiet)
                        {
                            Write-Verbose "Connection established..."
                            # If PSDrive already exists
                            If (Get-PSDrive -Name Philadelphia -ErrorAction SilentlyContinue)
                            {
                                Write-Verbose "Attempting to dismount PSDrive: $Server..."
                                # Dismounts drive
                                Remove-PSDrive Philadelphia
                                Write-Verbose "PSDrive: $Server has been dismounted."
                            }
                            Else
                            {
                                Write-Verbose "$Server does not exists as PSDrive..." -Verbose
                            }
                        }
                        Else
                        {
                            Write-Verbose "Connection could not be established, terminating..." -Verbose
                        }
                    }
                    # Dismounts the Sioux Falls PSDrive.
                    "Sioux"
                    {
                        # If server can be reached.
                        If (Test-Connection -computername ppsf-deploy-2 -count 1 -quiet)
                        {
                            Write-Verbose "Connection established..."
                            # If PSDrive already exists
                            If (Get-PSDrive -Name Sioux -ErrorAction SilentlyContinue)
                            {
                                Write-Verbose "Attempting to dismount PSDrive: $Server..."
                                # Dismounts drive
                                Remove-PSDrive Sioux
                                Write-Verbose "PSDrive: $Server has been dismounted."
                            }
                            Else
                            {
                                Write-Verbose "$Server does not exists as PSDrive..." -Verbose
                            }
                        }
                        Else
                        {
                            Write-Verbose "Connection could not be established, terminating..." -Verbose
                        }
                    }
                }
            }
            Catch
            {
                # Could not dismount server database as PSDrive 
                Write-Verbose "Could not dismount PSDrive: $server." -Verbose     
            }
        }	
	}
	End { } #End
}
# Retrieves the Asset Tag info from a Dell BIOS
Function Get-AssetTag
{
	<#
	.SYNOPSIS
		Returns the Asset tag from the computer queried by $ComputerName 
	.DESCRIPTION 
		Retrieves Asset Tag field from the Dell BIOS. Assigns the output to variable $AssetTag
	.PARAMETER ComputerName
		Computer name of the machien you would like to read Asset Tag information.
	.EXAMPLE
		Get-AsseTag -ComputerName Ws-De-SPA08888
		Queries computer with the name of WS-DE-SPA08888 and assigns the output to variable $AssetTag.
	.NOTES 
		Name: Get-AssetTag
		Author: Scott Goyette
		Date: 11/28/2015
		Email: sgoyette@thebancorp.com, sgoyette@netwow.biz
		Keywords: Asset Tag, AssetTag, Dell, Command Monitor
	#>
	[CmdLetbinding(SupportsShouldProcess = $True)]
	param
	(
		[Parameter (Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$True)]
		[String[]]$ComputerName
	)
	BEGIN {} #Begin
	PROCESS
	{
		ForEach ($Name in $ComputerName)
		{
			Try
            {
                # Host can be reached
                Write-Verbose "Connecting to $Name..."
                If (Test-Connection $Name -Count 1 -ErrorAction Stop)
                {
                    # Creates a new object with ComputerName and AssetTag properties
                    Write-Verbose "Retrieving Asset Tag from $Name..."
                    New-Object -TypeName PSObject -Property @{
				        "ComputerName" = $Name
				        "AssetTag" = (Get-WmiObject -ComputerName $Name -query "Select * from Win32_SystemEnclosure" -ErrorAction Stop).smbiosassettag
                    }
                }
            }
            Catch
            {
                Write-Verbose "Could not retrieve Asset Tag from $Name" -Verbose
            }
		}
	}
	End { } #End
}
# Retrieves the Dell Warranty Info
Function Get-DellWarranty
{
	<#
	.SYNOPSIS
		Get Warranty Info for Dell Computer
	.DESCRIPTION
		This takes a Computer Name, returns the ST of the computer,
   		connects to Dell's SOAP Service and returns warranty info and
   		related information. If computer is offline, no action performed.
   		ST is pulled via WMI.
	.PARAMETER
	.EXAMPLE
		Get-DellWarranty -Name bob, client1, client2 | ft -AutoSize
    	WARNING: bob is offline

    	ComputerName ServiceLevel  EndDate   StartDate DaysLeft ServiceTag Type                       Model ShipDate 
    	------------ ------------  -------   --------- -------- ---------- ----                       ----- -------- 
    	client1      C, NBD ONSITE 2/22/2017 2/23/2014     1095 7GH6SX1    Dell Precision WorkStation T1650 2/22/2013
    	client2      C, NBD ONSITE 7/16/2014 7/16/2011      334 74N5LV1    Dell Precision WorkStation T3500 7/15/2010
	.EXAMPLE
		Get-ADComputer -Filter * -SearchBase "OU=Exchange 2010,OU=Member Servers,DC=Contoso,DC=com" | get-dellwarranty | ft -AutoSize

    	ComputerName ServiceLevel            EndDate   StartDate DaysLeft ServiceTag Type      Model ShipDate 
   		------------ ------------            -------   --------- -------- ---------- ----      ----- -------- 
    	MAIL02       P, Gold or ProMCritical 4/26/2016 4/25/2011      984 CGWRNQ1    PowerEdge M905  4/25/2011
    	MAIL01       P, Gold or ProMCritical 4/26/2016 4/25/2011      984 DGWRNQ1    PowerEdge M905  4/25/2011
    	DAG          P, Gold or ProMCritical 4/26/2016 4/25/2011      984 CGWRNQ1    PowerEdge M905  4/25/2011
    	MAIL         P, Gold or ProMCritical 4/26/2016 4/25/2011      984 CGWRNQ1    PowerEdge M905  4/25/2011
	.EXAMPLE
		Get-DellWarranty -ServiceTag CGABCQ1,DGEFGQ1 | ft  -AutoSize

    	ServiceLevel            EndDate   StartDate DaysLeft ServiceTag Type      Model ShipDate 
    	------------            -------   --------- -------- ---------- ----      ----- -------- 
    	P, Gold or ProMCritical 4/26/2016 4/25/2011      984 CGABCQ1    PowerEdge M905  4/25/2011
    	P, Gold or ProMCritical 4/26/2016 4/25/2011      984 DGEFGQ1    PowerEdge M905  4/25/201
	.INPUTS
   		Name(ComputerName), ServiceTag
	.OUTPUTS
   		System.Object
	.NOTES 
		Name: Get-DellWarranty
		Author: Unkown - Maintainer Scott Goyette
		Date: 12/17/2015
		Email: sgoyette@thebancorp, sgoyette@netwow.biz
		Keywords: Dell Warranty, Dell, Warranty, Get-Warranty
	#Requires -Version 4.0
	#>	
	[CmdLetbinding(SupportsShouldProcess=$True)]
	[OutputType([System.Object])]
	param
	(
		# Name should be a valid computer name or IP address.
		[Parameter(Mandatory = $False,
				   ValueFromPipeline = $true,
				   ValueFromPipelineByPropertyName = $true,
				   ValueFromRemainingArguments = $false)]
		[Alias('HostName', 'Identity', 'DNSHostName', 'ComputerName')]
		[string[]]$Name,
		# ServiceTag should be a valid Dell Service tag. Enter one or more values.
		[Parameter(Mandatory = $false,
				   ValueFromPipeline = $false)]
		[string[]]$ServiceTag
	)
	BEGIN { } #Begin
	PROCESS
	{
        # Checks $ServiceTag for Value, should equal $Null in this case
		If ($ServiceTag -eq $Null)
		{
            Write-Verbose "Service Tag not entered..."
            # Attempts to retrieve warranty info for each computer name in $Name
			ForEach ($C in $Name)
			{
                Write-Verbose "Retrieving Dell warranty for $C..."
                Write-Verbose "Connecting to $C one moment..."
                # Tests connection to $C 
				$test = Test-Connection -ComputerName $c -Count 1 -Quiet
				If ($test -eq $true)
				{
                    # Test-Connection was good
                    Write-Verbose "Connection established..."
                    Write-Verbose "Looking up warranty status..."
                    # Attempting to retrieve the warranty info, connects to Dells website, uses WMI to get Service Tag
					$service = New-WebServiceProxy -Uri https://xserv.dell.com/services/assetservice.asmx?WSDL
					$system = Get-WmiObject -ComputerName $C win32_bios -ErrorAction SilentlyContinue
					$serial = $system.serialnumber
					$guid = [guid]::NewGuid()
					$info = $service.GetAssetInformation($guid, 'check_warranty.ps1', $serial)
					
					$Result = @{
						'ComputerName' = $c
						'ServiceLevel' = $info[0].Entitlements[0].ServiceLevelDescription.ToString()
						'EndDate' = $info[0].Entitlements[0].EndDate.ToShortDateString()
						'StartDate' = $info[0].Entitlements[0].StartDate.ToShortDateString()
						'DaysLeft' = $info[0].Entitlements[0].DaysLeft
						'ServiceTag' = $info[0].AssetHeaderData.ServiceTag
						'Type' = $info[0].AssetHeaderData.SystemType
						'Model' = $info[0].AssetHeaderData.SystemModel
						'ShipDate' = $info[0].AssetHeaderData.SystemShipDate.ToShortDateString()
				    }
					Write-Verbose "Warranty retrieved for $C..."
                    # Creates new object with retrieved data
					$obj = New-Object -TypeName psobject -Property $result
					Write-Output $obj
					
					$Result = $Null
					$system = $Null
					$serial = $null
					$guid = $Null
					$service = $Null
					$info = $Null
					$test = $Null
					$c = $Null
				}
				Else
				{
					Write-Warning "$c is offline"
					$c = $Null
				}
				
			}
		}
        # $ServiceTag has Value, should not equal $Null
		ElseIf ($ServiceTag -ne $Null)
		{
            Write-Verbose "Warranty lookup by Service Tag..."
			ForEach ($S in $ServiceTag)
			{
                # Prepares to lookup warranty info by Service Tags entered
                Write-Verbose "Looking up warranty by Service Tag for $S..."
				$service = New-WebServiceProxy -Uri https://xserv.dell.com/services/assetservice.asmx?WSDL
				$guid = [guid]::NewGuid()
				$info = $service.GetAssetInformation($guid, 'check_warranty.ps1', $S)
				
				If ($info -ne $Null)
				{
					
					$Result = @{
						'ServiceLevel' = $info[0].Entitlements[0].ServiceLevelDescription.ToString()
						'EndDate' = $info[0].Entitlements[0].EndDate.ToShortDateString()
						'StartDate' = $info[0].Entitlements[0].StartDate.ToShortDateString()
						'DaysLeft' = $info[0].Entitlements[0].DaysLeft
						'ServiceTag' = $info[0].AssetHeaderData.ServiceTag
						'Type' = $info[0].AssetHeaderData.SystemType
						'Model' = $info[0].AssetHeaderData.SystemModel
						'ShipDate' = $info[0].AssetHeaderData.SystemShipDate.ToShortDateString()
					}
				}
				Else
				{
					Write-Warning "$S is not a valid Dell Service Tag."
				}
				Write-Verbose "Warranty retrieved for Service Tag: $S..."
                # Creates new object with retrieved data
				$obj = New-Object -TypeName psobject -Property $result
				Write-Output $obj
				
				$Result = $Null
				$system = $Null
				$serial = $Null
				$guid = $Null
				$service = $Null
				$s = $Null
				$info = $Null
				
			}
		}
	}
	End { } #End
}
# Retrieves whether a computers copy of Windows is Genuine
Function Get-Genuine
{
	<#
	.SYNOPSIS
		Assigns a global variable $ActivationStatus the valuse of LicenseStatus. If it equals 1 then it is activated.
	.DESCRIPTION
		Using WmiObject finds the value of LicenseStatus, if equaled to 1 then it is a Genuine copy of Windows.
	.PARAMETER ComputerName
	.EXAMPLE
	.NOTES 
		Name: Get-Genuine
		Author: Scott Goyette
		Date: 11/29/2015
		Email: sgoyette@thebancorp.com, sgoyette@netwow.biz
		Keywords: Genuine, Activation, Windows, $ActivationStatus
	#Requires -Version 4.0
	#>
	[CmdLetbinding(SupportsShouldProcess=$True)]
	param
	(
		[Parameter (Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$true)]
		[String[]]$ComputerName
	)
	BEGIN {} #Begin
	PROCESS
	{
        ForEach ($Name in $ComputerName)
        {
            Try
            {
                Write-Verbose "Attempting to connect to $Name..."
                # Tests connection to computer
                If (Test-Connection $Name -Count 1 -ErrorAction Stop)
                {
                    Write-Verbose "Retrieving Windows Genuine info from $Name..."
                    # Creates new object with ComputerName and Genuine properties. A return value of 1 for Genuine means it is a genuine copy of Windows
                    New-Object -TypeName PSObject -Property @{
                        "ComputerName" = $Name
                        "Genuine" = Get-WmiObject SoftwarelicensingProduct -ComputerName $Name -ErrorAction Stop | where ApplicationID -EQ 55c92734-d682-4d71-983e-d6ec3f16059f | where licensestatus -eq 1 | select -ExpandProperty licensestatus
                    }
                }
            }
            Catch
            {
                Write-Verbose "Could not retrieve Windows Genuine info from $Name." -Verbose
            }
        } 
	}
	End { } #End
}
# Retrieves the Service Tag from a Dell BIOS
Function Get-ServiceTag
{
	<#
	.SYNOPSIS
		Assigns the variable $ServiceTag the Service Tag from the Computer queried.
	.DESCRIPTION 
		Retrieves Service Tag of a Dell computer.
	.PARAMETER ComputerName
		Computer name to query for Service Tag
	.EXAMPLE
		Get-ServiceTag -ComputerName WS-DE-SPA08888
		Retrieves  Service Tag found in the Dell BIOS from computer named WS-DE-SPA08888
	.NOTES 
		Name: Get-ServiceTag
		Author: Scott Goyette
		Date: 11/28/2015
		Email: sgoyette@thebancorp.com, sgoyette@netwow.biz
		Keywords: Service Tag, ServiceTag, Dell, Command Monitor
	#>
	[CmdLetbinding(SupportsShouldProcess=$True)]
	param
	(
		[Parameter (Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$true)]
		[String[]]$ComputerName
	)
	BEGIN {} #Begin
	PROCESS
	{
		ForEach ($Name in $ComputerName)
		{
            Try
            {
                # Test connection to computer
                Write-Verbose "Connecting to $Name..."
                If (Test-Connection $Name -count 1 -ErrorAction Stop)
                {
                    # Creates a new object with ComputerName and ServiceTag properties
                    Write-Verbose "Retrieving Service Tag from $Name..."
                    New-Object -TypeName PSObject -Property @{
				        "ComputerName" = $Name
				        "ServiceTag" = gwmi -ComputerName $Name dcim_chassis -namespace root\dcim\sysman tag -ErrorAction Stop | select -ExpandProperty Tag
			        }
                }
            }
            Catch
            {
                Write-Verbose "Could not retrieve Service Tag for $Name." -Verbose
            }
			
		}
	}
	End { } #End
}
# Retrieves a listing of Organizational Units in AD based on Hardware Type
Function Get-SiteOU
{
	<#
	.SYNOPSIS
		Displays The Bancorp's site locations based on 3 criteria All, Laptops, or Workstations.
	.DESCRIPTION
		The Get-SiteOU displays site location OU's based on hardware type. Desktop Support
		only needs to worry about Laptops and Workstations.
	.PARAMETER HardwareType
		Based on 3 criteria All, Laptops, or Workstations
	.EXAMPLE
		Get-SiteOU -HardwareType All
		Will assign to the variable $SiteOU all the Site OU in Active Directory.
	.EXAMPLE
		Get-SiteOU -HardwareType Laptops
		Will assign to the variable $SiteOU all current Laptop Site OU in AActive Directory.
	.EXAMPLE
		Get-SiteOU -HardwareType Workstations
		Will assign to the variable $SiteOU all current Workstation Site OU in Acitve Directory.
	.NOTES 
		Name: Get-SiteOU
		Author: Scott Goyette
		Date: 10/21/2015
		Email: sgoyette@thebancorp.com, sgoyette@netwow.biz
		Keywords: Hardware Type, OU, Get-SiteOU, Acitve Dircetory
	#Requires -Version 4.0
	#>
	[CmdLetBinding(SupportsShouldProcess=$True)]
	param
	(
		[Parameter (Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$true)]
	   [String[]]$HardwareType
	)  
	begin{}#Begin
	process
	{
		Switch ($HardwareType)
		{
			# Generates a site OU listing for the Laptops Hardware Type, assigns to global variable $SiteOU.
			Laptops
			{
				$Laptops = Get-ADOrganizationalUnit -Filter 'name -like "*"' -SearchBase "OU=$HardwareType, OU=hardware,OU=bancorp,DC=thebancorp,DC=local" -SearchScope OneLevel | Select-Object -expand Name
				ForEach ($OU in $Laptops)
				{
					New-Object -TypeName PSObject -Property @{
						"Site" = $OU
					}
				}
			}
			# Generates a site OU listing for the Workstations Hardware Type, assigns to global variable $SiteOU.
			Workstations
			{
				$Workstations = Get-ADOrganizationalUnit -Filter 'name -like "*"' -SearchBase "OU=$HardwareType, OU=hardware,OU=bancorp,DC=thebancorp,DC=local" -SearchScope OneLevel | Select-Object -expand Name
				ForEach ($OU in $Workstations)
				{
					New-Object -TypeName PSObject -Property @{
						"Site" = $OU
					}
				}
			}
			# Generates a site OU listing for the Workstations Hardware Type and the Laptops Hardware Type. Compares the two lists and only puts the unique names from the list, 
			# so only one Sioux Falls or Wilmington will report. Combines them into global variable $SiteOU
			All
			{
				$Laptops = Get-ADOrganizationalUnit -Filter 'name -like "*"' -SearchBase "OU=Laptops, OU=hardware,OU=bancorp,DC=thebancorp,DC=local" -SearchScope OneLevel | Select-Object -expand Name
				$Workstations = Get-ADOrganizationalUnit -Filter 'name -like "*"' -SearchBase "OU=Workstations, OU=hardware,OU=bancorp,DC=thebancorp,DC=local" -SearchScope OneLevel | Select-Object -expand Name
				$All = $Workstations + $Laptops | select -Unique | sort
				ForEach ($OU in $All)
				{
					New-Object -TypeName PSObject -Property @{	
						"Site" = $OU	
					}	
				}
	    	}
	    }
	}
	End{}#End
}
# Mounts a DSM server as PSDrive
Function Mount-DSM
{
	<#
	.SYNOPSIS
		Mounts the DSM Server based on Parameter passed to a PSDrive of same name.
	.DESCRIPTION
		Cmdlet must be run from a DSM server node currently. Mounts server databases as a PSDrive that
        can be explored as you would any directory.
	.PARAMETER Server
	
	.EXAMPLE
		Mount-DSM Bulgaria
        Mounts the Bulgaria DSM server to a PSDrive named Bulgaria
	.EXAMPLE
		Mount-DSM Delaware
        Mounts the Delaware DSM server to a PSDrive named Delaware
	.EXAMPLE
	    Mount-DSM Philadelphia
        Mounts the Philadelphia DSM server to a PSDrive named Philadelphia
    .EXAMPLE
        Mount-DSM Sioux
        Mounts the Sioux Falls DSM server to a PSDrive named Sioux
	.NOTES
		Name: Mount-DSM
		Author: Scott Goyette 
		Date: 03/13/2016
		Email: sgoyette@thebancorp.com, scottcgoyette@yahoo.com 
		Keywords: Mount, DSM
	#>	
	[CmdLetbinding(SupportsShouldProcess=$True)]
	[OutputType([System.Object])]
	param
	(
        [Parameter (Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$true)]
		[ValidateSet("Delaware", "Philadelphia", "Sioux", "Bulgaria")]
        [String[]]$Server
	)
	BEGIN {} #Begin
	PROCESS
	{
	    ForEach ($Name in $Server) 
        {
            Try
            {
                Write-Verbose "Establishing Connection to $Server DSM server..."
                switch($Server)
                {
                    # Mounts Bulgaria DSM server.
                    "Bulgaria"
                    {
                        # If server can be reached
                        If (Test-Connection -computername vpbg2-dsm-1 -count 1 -quiet)
                        {
                            Write-Verbose "Connection established..."
                            # If PSDrive already exists
                            If (Get-PSDrive -Name Bulgaria -ErrorAction SilentlyContinue)
                            {
                                Write-Verbose "$Server DSM server already exists as PSDrive..." -Verbose
                            }
                            else
                            {
                                Write-Verbose "Attempting to mount $Server DSM server as PSDrive..."
                                # Drive doesnt exist attempt to mount
                                New-PSDrive -Name Bulgaria -PSProvider BlsEmdb -Root \ -blshost \\vpbg2-dsm-1 -Scope Global -ErrorAction Stop
                                Write-Verbose "Mounted. To enter drivespace type `"Bulgaria:`""
                            }
                        }
                        else
                        {
                            Write-Verbose "Connection could not be established, terminating..." -Verbose
                        }
                    }
                    # Mounts Delaware DSM server.
                    "Delaware" 
                    {
                        # If server can be reached.
                        If (Test-Connection -computername vpwi-dsm-1 -count 1 -quiet)
                        {
                            Write-Verbose "Connection established..."
                            # If PSDrive exists.
                            If (Get-PSDrive -Name Delaware -ErrorAction SilentlyContinue)
                            {
                                Write-Verbose "$Server DSM server already exists as PSDrive..." -Verbose
                            }
                            else
                            {
                                Write-Verbose "Attempting to mount $Server DSM server as PSDrive..."
                                # PSDrive doesn't exist, attempts to mount.
                                New-PSDrive -Name Delaware -PSProvider BlsEmdb -Root \ -blshost \\vpwi-dsm-1 -Scope Global -ErrorAction Stop
                                Write-Verbose "Mounted. To enter drivespace type `"Delaware:`""
                            }
                        }
                        else
                        {
                            Write-Verbose "Connection could not be established, terminating..." -Verbose
                        }
                        
                    }
                    # Mounts Sioux Falls DSM server.
                    "Sioux"
                    {
                        # If server can be reached.
                        If (Test-Connection -computername ppsf-deploy-2 -count 1 -quiet)
                        {
                            # If PSDrive exists
                            Write-Verbose "Connection established..."
                            If (Get-PSDrive -Name Sioux -ErrorAction SilentlyContinue)
                            {
                                Write-Verbose "$Server DSM server already exists as PSDrive..." -Verbose
                            }
                            else
                            {
                                Write-Verbose "Attempting to mount $Server DSM server as PSDrive..."
                                # PSDrive doesn't exist, attempts to mount.
                                New-PSDrive -Name Sioux -PSProvider BlsEmdb -Root \ -blshost \\ppsf-deploy-2 -Scope Global -ErrorAction Stop
                                Write-Verbose "Mounted. To enter drivespace type `"Sioux:`""
                            }
                        }
                        else
                        {
                            Write-Verbose "Connection could not be established, terminating..." -Verbose
                        }
                    }
                    # Mounts Philadelphia DSM server.
                    "Philadelphia"
                    {
                        # If server can be reached.
                        If (Test-Connection -computername ppph-deploy-1 -count 1 -quiet)
                        {
                            # If PSDrive exists.
                            Write-Verbose "Connection established..."
                            If (Get-PSDrive -Name Philadelphia -ErrorAction SilentlyContinue)
                            {
                                Write-Verbose "$Server DSM server already exists as PSDrive..." -Verbose
                            }
                            else
                            {
                                Write-Verbose "Attempting to mount $Server DSM server as PSDrive..."
                                # PSDrive doesn't exist, attempts to mount
                                New-PSDrive -Name Philadelphia -PSProvider BlsEmdb -Root \ -blshost \\ppph-deploy-1 -Scope Global -ErrorAction Stop
                                Write-Verbose "Mounted. To enter drivespace type `"Philadelphia:`""
                            }
                        }
                        else
                        {
                            Write-Verbose "Connection could not be established, terminating..." -Verbose
                        }
                    }
                }
            }
            catch
            {
                # Could not mount server database as PSDrive 
                Write-Verbose "Could not mount $server DSM server as PSDrive" -Verbose    
            }
        }	
	} # Prcoess
	End {} #End
}
# Creates a computer object in an OU based on computer naming standards
Function New-ComputerProperOU
{
	<#
	.SYNOPSIS
		Removing the need to move the computer to correct OU after DSM joining it to domain. Will automatically
		create a computer placeholder object in the correct OU based on current computer naming standards.
	.DESCRIPTION
		Creates a computer object placeholder in OU based on the Computer Naming Standard automatically. When computer
		ia joined to domain it is in the correct OU.
	.PARAMETER ComputerName
		Computer name of the computer object to create in the OU based on the Computer Naming Standard
	.EXAMPLE
		New-ComputerProperOU -ComputerName WS-DE-SPA08888
		Will create a computer object placeholder in Acitive Directory with the name WS-DE-SPA08888 in the Wilmington Workstations OU
	.NOTES 
		Name: New-ComputerProperOU
		Author: Scott Goyette
		Date: 11/28/2015
		Email: sgoyette@thebancorp.com, sgoyette@netwow.biz
		Keywords: Bancorp, Computer, OU, Correct, Create, New-Computer
	#Requires -Version 4.0
	#>
	[CmdLetbinding(SupportsShouldProcess=$True)]
	param
	(
		[Parameter (Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$true)]
		[String[]]$ComputerName
	)
	BEGIN {} #Begin
	PROCESS
	{
		foreach ($Name in $ComputerName)
		{
			# Splits the $ComputerName variable into manageable chunks
			Split-ComputerName $Name
			# Creates new computer object in Active Directory in the proper OU based on naming standards
			function New-Computer ($Site, $HardwareType)
			{
				New-ADComputer -Name "$Name" -SAMAccountName "$Name" -Enabled $true -Path "Ou=$Site, OU=$HardwareType, OU=Hardware, OU=Bancorp, DC=thebancorp, DC=local"
			}
			# Find out what Hardware equals then assigns $HardwareType either Laptops or Workstations
			switch ($Computer.Hardware)
			{
				"LT"	{ $HardwareType = "Laptops" }
				"TB"	{ $HardwareType = "Laptops" }
				"WS"	{ $HardwareType = "Workstations" }
				#default { "Hardware Unkown" }
			}
			# Find out what Location contains and compare to assign $Site the correct Site OU
			switch ($Computer.Location)
			{
				"BUL"	{ $Site = "Sofia" }
				"CH"	{ $Site = "Chicago" }
				"DE"	{ $Site = "Wilmington" }
				"FL"	{ $Site = "Orlando" }
				"GIB"	{ $Site = "Gibraltar" }
				"KE"	{ $Site = "Kent" }
				"KOP"	{ $Site = "KingOfPrussia" }
				"MD"	{ $Site = "Crofton" }
				"MN"	{ $Site = "Minneapolis" }
                "NY"    { $Site = "New York" }
				"PHL"	{ $Site = "Philadelphia" }
				"RU"	{ $Site = "Remote Sites"; $HardwareType = "Laptops" }
				"SF"	{ $Site = "Sioux Falls" }
                "TA"    { $Site = "Tampa" }
			}
			New-Computer $Site $HardwareType
		}
	}
	End { } #End
}
# Creates a New Computer Object in DSM
Function New-DSMComputer
{
	<#
	.SYNOPSIS
        Creates a new computer object in DSM and marks ready for deployment
	.DESCRIPTION
        Creates a new computer object in DSM with a few criteria: Names the computer object, assigns an initial Mac Address to the computer object,
        places the computer object into the correct OU structure of DSM based on hardware type and location, marks as active in the DSM console.
	.PARAMETER ComputerName

    .PARAMETER MacAddress

	.EXAMPLE
        New-DSMComputer WS-DE-5290 989096E0F198
        This will create a new computer object in the Workstations\WIlmington OU and mark it as active.
	.NOTES 
	    Name: New-DSMComputer
	    Author: Scott Goyette
	    Date: 03/14/2016
	    Email: sgoyette@thebancorp.com, scottcgoyette@yahoo.com
	    Keywords: DSM Computer, New
	#>
	[CmdLetbinding(SupportsShouldProcess=$True)]
	param
	(
        [Parameter (Position=0, Mandatory=$True,
					ValueFromPipeline=$True,
					ValueFromPipelineByPropertyName=$True)]
		[String[]]$ComputerName,
        [Parameter(Position=1, Mandatory=$true, ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)]
        [ValidateScript({
            $pattern = @(
            '^([0-9a-f]{12})$'
            )
            if ($_ -match $pattern) {$true} else {
                    throw "The argument '$_' does not match a valid MAC address format."
            }
        })]
        [string]$MacAddress
	)
	BEGIN
	{
        # Section moves into the correct PSDrive space based on hostname executing command
        $ExecutionHost = $env:COMPUTERNAME
        Write-Verbose "Executing command from $ExecutionHost..."
        Split-ComputerName $ComputerName
        switch($ExecutionHost)
        {
            "PPPH-DEPLOY-1"
            {
                Mount-DSM Philadelphia
                Write-verbose "Philadelphia: Mounted accessing drive..."
                switch($Computer.Hardware)
                {
                    "LT" {cd "Philadelphia:\RootDSE\Managed Users & Computers\Laptops"; $Server ="Philadelphia:\RootDSE\Managed Users & Computers\Laptops"}
                    "TB" {cd "Philadelphia:\RootDSE\Managed Users & Computers\Laptops"; $Server ="Philadelphia:\RootDSE\Managed Users & Computers\Laptops"}
                    "WS" {cd "Philadelphia:\RootDSE\Managed Users & Computers\Workstations"; $Server ="Philadelphia:\RootDSE\Managed Users & Computers\Workstations"}
                }
            }
            "PPSF-DEPLOY-2"
            {
                Mount-DSM Sioux
                Write-verbose "Sioux: Mounted accessing drive..."
                switch($Computer.Hardware)
                {
                    "LT" {cd "Sioux:\RootDSE\Managed Users & Computers\Laptops"; $Server ="Sioux:\RootDSE\Managed Users & Computers\Laptops"}
                    "TB" {cd "Sioux:\RootDSE\Managed Users & Computers\Laptops"; $Server ="Sioux:\RootDSE\Managed Users & Computers\Laptops"}
                    "WS" {cd "Sioux:\RootDSE\Managed Users & Computers\Workstations"; $Server ="Sioux:\RootDSE\Managed Users & Computers\Workstations"}
                }    
            }
            "VPBG2-DSM-1"
            {
                Mount-DSM Bulgaria
                Write-verbose "Bulgaria: Mounted accessing drive..."
                switch($Computer.Hardware)
                {
                    "LT" {cd "Bulgaria:\RootDSE\Managed Users & Computers\Laptops"; $Server ="Bulgaria:\RootDSE\Managed Users & Computers\Laptops"}
                    "TB" {cd "Bulgaria:\RootDSE\Managed Users & Computers\Laptops"; $Server ="Bulgaria:\RootDSE\Managed Users & Computers\Laptops"}
                    "WS" {cd "Bulgaria:\RootDSE\Managed Users & Computers\Workstations"; $Server ="Bulgaria:\RootDSE\Managed Users & Computers\Workstations"}
                }    
            }
            "VPWI-DSM-1"
            {
                Mount-DSM Delaware
                Write-verbose "Delaware: Mounted accessing drive..."
                switch($Computer.Hardware)
                {
                    "LT" {cd "Delaware:\RootDSE\Managed Users & Computers\Laptops"; $Server ="Delaware:\RootDSE\Managed Users & Computers\Laptops"}
                    "TB" {cd "Delaware:\RootDSE\Managed Users & Computers\Laptops"; $Server ="Delaware:\RootDSE\Managed Users & Computers\Laptops"}
                    "WS" {cd "Delaware:\RootDSE\Managed Users & Computers\Workstations"; $Server ="Delaware:\RootDSE\Managed Users & Computers\Workstations"}
                }
            }
        }
    }#Begin
    PROCESS
	{
        Try
        {
            # Grabs the site location for our hardware
            Switch ($Computer.Location)
			{
				"BUL"	{ $Site = "Sofia" }
				"CH"	{ $Site = "Chicago" }
				"DE"	{ $Site = "Wilmington" }
				"FL"	{ $Site = "Orlando" }
				"GIB"	{ $Site = "Gibraltar" }
				"KE"	{ $Site = "Kent" }
				"KOP"	{ $Site = "KingOfPrussia" }
				"MD"	{ $Site = "Crofton" }
				"MN"	{ $Site = "Minneapolis" }
                "NY"    { $Site = "New York" }
				"PHL"	{ $Site = "Philadelphia" }
				"RU"	{ $Site = "Remote Sites"}
				"SF"	{ $Site = "Sioux Falls" }
                "TA"    { $Site = "Tampa" }
			}
            Write-Verbose "Ensuring computer object $ComputerName does not already exist..."
            If (Get-EmdbComputer ".\$Site\$ComputerName")
            {
                Write-Verbose "Computer with the name: $ComputerName, already exists." -verbose
            }
            Else
            {
                # Begins the computer object creation process.
                Write-Verbose "Creating computer object with name: $ComputerName and initial Mac Address: $MacAddress, in OU $Server\$Site"
                cd "$Server\$Site"
                Write-Verbose "Object creation..."
                $DSMComputer = New-Item $ComputerName -ItemType "Computer" -DelayCreation
                Write-Verbose "Assigning Mac Address..."
                $DSMComputer.InitialMACAddress = $MacAddress
                Write-Verbose "Activating computer object..." 
                $DSMComputer.OperationMode = "Active"
                Write-Verbose "Finalizing..."
                $DSMComputer.Create()
                Write-Verbose "Computer object created, ready to begin deploying packages..."
            }  	
        }
        Catch
        {
            Write-Verbose "Computer object could not be created due to existing object with Mac Address." -Verbose
        }
	}
	End { } #End
}
# Removes DSM computer object from Core Apps group
Function Remove-CoreApps
{
	<#
	.SYNOPSIS
        Removes the computer from CoreApps group in DSM.
	.DESCRIPTION
        Must be executed from one of the DSM server nodes. Removes the computer from CoreApps group in DSM.
	.PARAMETER ComputerName

	.EXAMPLE
        Remove-CoreApps WS-DE-5290
        Removes the computer from the CoreApps group.
	.NOTES 
	    Name: Remove-CoreApps
	    Author: Scott Goyette
	    Date: 03/22/2016
	    Email: sgoyette@thebancorp.com, scottcgoyette@yahoo.com
	    Keywords: 
	#>
	[CmdLetbinding(SupportsShouldProcess=$True)]
	param
	(
        [Parameter (Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$true)]
		[String[]]$ComputerName
	)
	BEGIN
	{
        # Section moves into the correct PSDrive space based on hostname executing command
        $ExecutionHost = $ENV:COMPUTERNAME
        Write-Verbose "Executing command from $ExecutionHost..."
        Split-ComputerName $ComputerName
        switch($ExecutionHost)
        {
            "PPPH-DEPLOY-1"
            {
                Mount-DSM Philadelphia
                Write-verbose "Philadelphia: Mounted accessing drive..."
                $Server ="Philadelphia:\RootDSE\Managed Users & Computers\OS Deployment"
            }
            "PPSF-DEPLOY-2"
            {
                Mount-DSM Sioux
                Write-verbose "Sioux: Mounted accessing drive..."
                $Server ="Sioux:\RootDSE\Managed Users & Computers\OS Deployment"    
            }
            "VPBG2-DSM-1"
            {
                Mount-DSM Bulgaria
                Write-verbose "Bulgaria: Mounted accessing drive..."
                $Server ="Bulgaria:\RootDSE\Managed Users & Computers\OS Deployment"    
            }
            "VPWI-DSM-1"
            {
                Mount-DSM Delaware
                Write-verbose "Delaware: Mounted accessing drive..."
                $Server ="Delaware:\RootDSE\Managed Users & Computers\OS Deployment"
            }
        }
    }#Begin
	PROCESS
	{
        Try
        {
            # Verifies if computer is found in DSM, then removes the computer from the CoreApps package group.
            Write-Verbose "Ensuring computer object exists..."
            If (Get-EmdbComputer -Filter "(&(name=$ComputerName))" -recurse)
            {
                # Removes computer from CoreApps group membership.
                Write-Verbose "Attempting to remove $ComputerName from CoreApps group..."
                $CoreApps = Get-EmdbGroup "$Server\!Group- Core Applications" -SchemaTag "Group"
                $Computer = Get-EmdbComputer -Filter "(&(name=$ComputerName))" -recurse
                Remove-EmdbGroupMember -Group $CoreApps -Member $Computer
                Write-Verbose "$ComputerName removed from CoreApps group."
            }
        }
        Catch
        {
            # Computer doesn't exist or couldnt remove CoreApps group from computer.
            Write-Verbose "Could not remove CoreApps package from $ComputerName or computer does not exist." -Verbose
        }		
	}
	End { } #End
}
# Remove a DSM Computer from the server.
Function Remove-DSMComputer
{
	<#
	.SYNOPSIS
        Removes the computer object specified off the DSM nodes. Destroys object and all packages.
	.DESCRIPTION
        Removes the computer object specified and all properties off the DSM nodes. Destroys all association with that computer from DSM servers.
	.PARAMETER ComputerName

	.EXAMPLE
        Remove-DSMComputer WS-DE-5290
        Will remove the computer WS-DE-5290 from all DSM server nodes. Object will cease to exist and deploy further.
	.NOTES 
	    Name: Remove-DSMComputer
	    Author: Scott Goyette
	    Date: 03/21/2016
	    Email: sgoyette@thebancorp.com, scottcgoyette@yahoo.com
	    Keywords: Remove, DSMComputer, Computer, DSM
	#>
	[CmdLetbinding(SupportsShouldProcess=$True)]
	param
	(
        [Parameter (Position=0, Mandatory=$True,
					ValueFromPipeline=$True,
					ValueFromPipelineByPropertyName=$True)]
		[String[]]$ComputerName
	)
	BEGIN
	{
        # Section moves into the correct PSDrive space based on hostname executing command
        $ExecutionHost = $env:COMPUTERNAME
        Write-Verbose "Executing command from $ExecutionHost..."
        Split-ComputerName $ComputerName
        switch($ExecutionHost)
        {
            "PPPH-DEPLOY-1"
            {
                Mount-DSM Philadelphia
                Write-verbose "Philadelphia: Mounted, accessing drive..."
            }
            "PPSF-DEPLOY-2"
            {
                Mount-DSM Sioux
                Write-verbose "Sioux: Mounted, accessing drive..."    
            }
            "VPBG2-DSM-1"
            {
                Mount-DSM Bulgaria
                Write-verbose "Bulgaria: Mounted, accessing drive..."    
            }
            "VPWI-DSM-1"
            {
                Mount-DSM Delaware
                Write-verbose "Delaware: Mounted, accessing drive..."
            }
        }
    }#Begin
	PROCESS
	{
	    Try
        {
            Write-Verbose "Ensuring computer object exists..."
            If (Get-EmdbComputer -Filter "(&(name=$ComputerName))" -recurse)
            {
                # Deletes the computer from DSM servers.
                Write-Verbose "Attempting to remove computer object..."
                Remove-EmdbComputer -Filter "(&(name=$computerName))" -Recurse
                Write-Verbose "$ComputerName deleted from DSM server."
            }   
        }
        Catch
        {
            # Computer object does not exist in DSM
            Write-Verbose "Computer object with name: $Computername does not exist." -Verbose
        }	
	}
	End { } #End
}
# Removes DSM computer object from OS package group
Function Remove-OSPackage
{
	<#
	.SYNOPSIS
        Removes the OS packages from target computer in DSM.
	.DESCRIPTION
        Must be executed from one of the DSM server nodes. Will remove all OS packages from the target computer in DSM
	.PARAMETER ComputerName

	.EXAMPLE
      Remove-OSPackage WS-DE-5290
      Will rempove all OS packages from computer object targeted.  
	.NOTES 
	    Name: Remove-OSPackage
	    Author: Scott Goyette
	    Date: 03/28/2016
	    Email: sgoyette@thebancorp.com, scottcgoyette@yahoo.com
	    Keywords: 
	#>
	[CmdLetbinding(SupportsShouldProcess=$True)]
	param
	(
        [Parameter (Position=0, Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$True)]
		[String[]]$ComputerName
	)
	BEGIN
	{
        # Section moves into the correct PSDrive space based on hostname executing command
        $ExecutionHost = $ENV:COMPUTERNAME
        Write-Verbose "Executing command from $ExecutionHost..."
        Split-ComputerName $ComputerName
        switch($ExecutionHost)
        {
            "PPPH-DEPLOY-1"
            {
                Mount-DSM Philadelphia
                Write-verbose "Philadelphia: Mounted accessing drive..."
                $Server ="Philadelphia:\RootDSE\Managed Users & Computers\OS Deployment"
            }
            "PPSF-DEPLOY-2"
            {
                Mount-DSM Sioux
                Write-verbose "Sioux: Mounted accessing drive..."
                $Server ="Sioux:\RootDSE\Managed Users & Computers\OS Deployment"    
            }
            "VPBG2-DSM-1"
            {
                Mount-DSM Bulgaria
                Write-verbose "Bulgaria: Mounted accessing drive..."
                $Server ="Bulgaria:\RootDSE\Managed Users & Computers\OS Deployment"    
            }
            "VPWI-DSM-1"
            {
                Mount-DSM Delaware
                Write-verbose "Delaware: Mounted accessing drive..."
                $Server ="Delaware:\RootDSE\Managed Users & Computers\OS Deployment"
            }
        }
    }#Begin
	PROCESS
	{
        Try
        {
            # Verifies if computer is found in DSM, then removes the computer from the OS package group.
            Write-Verbose "Ensuring computer object exists..."
            If (Get-EmdbComputer -Filter "(&(name=$ComputerName))" -recurse)
            {
                Write-Verbose "Finding OS package installation."
                $ComputerPackages = Get-EmdbComputer -Filter "(&(name=$ComputerName))" -recurse
                $PackageArray = $ComputerPackages.GetStaticGroupMemberships() | Select -expand Name
                Switch ($PackageArray)
                {
                    
                    "Windows 7 (x64) Pro"
                    {
                        # Windows 7 64 Bit Professional
                        $OS = Get-EmdbGroup "$Server\Windows 7 (x64) Pro" -SchemaTag "Group"
                        Remove-EmdbGroupMember -Group $OS -Member $ComputerPackages
                        Write-Verbose "OS Package removed."

                    }
                    "Windows 7 (x86) Pro" 
                    {
                        # 32 Bit Windows 7 Pro
                        $OS = Get-EmdbGroup "$Server\Windows 7 (x86) Pro" -SchemaTag "Group"
                        Remove-EmdbGroupMember -Group $OS -Member $ComputerPackages
                        Write-Verbose "OS Package removed."
                    }
                    "Windows 8.1 (x64) Pro"
                    {
                        # Windows 8.1 64 Bit
                        $OS = Get-EmdbGroup "$Server\Windows 8.1 (x64) Pro" -SchemaTag "Group"
                        Remove-EmdbGroupMember -Group $OS -Member $ComputerPackages
                        Write-Verbose "OS Package removed."
                    }       
                
                }
            }
        }
        Catch
        {
            Write-Verbose "Could not remove OS package from $ComputerName or computer does not exist." -Verbose
        }        		
	}
	End { } #End
}
# Removes DSM computer object from software package group
function Remove-SoftwarePackage
{
	<#
	.SYNOPSIS
        Removes computer object from the software package specified. Software package must be wrapped in quotes "Software Name" to account for spaces.
	.DESCRIPTION
		Must be executed from a DSM server node. Removes the computer ojbect specified from the software package group specified. If you do not know the software
        package to remove you can output a listing of the current one off packages that are deployable.

	.PARAMETER ComputerName

    .PARAMETER SoftwarePackage

    .PARAMETER List
	
	.EXAMPLE
        Remove-SoftwarePackage -List
        Retrieves a current list of the one offs that can be deployed.

    .EXAMPLE
        Remove-SfotwarePackage WS-DE-5290 "Adobe Acrobat Standard DC"
        Removes the computer from the group Adobe Acrobat Standard DC.

    .EXAMPLE
        Remove-SoftwarePackage WS-DE-5290 "Adobe"
        Will suggest all products named Adobe or have Adobe in them.
				
	.NOTES 
		Name: Remove-SoftwarePackage
		Author: Scott Goyette
		Date: 04/14/2016
		Email: sgoyette@thebancorp.com, scottcgoyette@yahoo.com
		Keywords: Remove, Software, Package, SoftwarePackage 
	#>	
	[CmdLetbinding(SupportsShouldProcess=$True)]
	param
	(
		[Parameter (Position=0, Mandatory=$False,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$True)]
		[String[]]$ComputerName,
        [Parameter (Position=1, Mandatory=$False,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$True)]
        [String[]]$SoftwarePackage,
        
        [Switch]$List
	)
	BEGIN 
    {
        # Section moves into the correct PSDrive space based on hostname executing command
        $ExecutionHost = $ENV:COMPUTERNAME
        Write-Verbose "Executing command from $ExecutionHost..."
        switch($ExecutionHost)
        {
            "PPPH-DEPLOY-1"
            {
                Mount-DSM Philadelphia
                Write-verbose "Philadelphia: Mounted accessing drive..."
                $Server ="Philadelphia:\RootDSE\Managed Users & Computers\"
            }
            "PPSF-DEPLOY-2"
            {
                Mount-DSM Sioux
                Write-verbose "Sioux: Mounted accessing drive..."
                $Server ="Sioux:\RootDSE\Managed Users & Computers\"    
            }
            "VPBG2-DSM-1"
            {
                Mount-DSM Bulgaria
                Write-verbose "Bulgaria: Mounted accessing drive..."
                $Server ="Bulgaria:\RootDSE\Managed Users & Computers\"    
            }
            "VPWI-DSM-1"
            {
                Mount-DSM Delaware
                Write-verbose "Delaware: Mounted accessing drive..."
                $Server ="Delaware:\RootDSE\Managed Users & Computers\"
            }
        }
        # Gets a listing of current Software One-offs.
        $SoftwarePackageList = Get-emdbGroup "$Server\Software\Software- One offs\*" -SchemaTag "Group" | sort
        # If the $List switch is set at Cmdlet run, will output a listing of all applications we have for
        # Software one offs.
        If ($List -eq $True)
        {
            Write-Output $SoftwarePackageList | select -ExpandProperty Name
            Break
        }
    } #Begin
	PROCESS
	{
	    Set-Location $Server
        # Verifies a Computer name was typed in.
        If ($ComputerName -eq $Null)
        {
            Write-Verbose "Must specify a computer name to add to SoftwarePackage group." -Verbose
            Break
        }
        # Verifies that computer name specified exists in DSM as a computer object.
        Write-Verbose "Verifying computer exists on DSM server..."
        If (Get-EmdbComputer "$ComputerName" -recurse)
        {
            Write-Verbose "Verifying you chose a software package in our software list"
            # Verifies that the $SoftwarePackage variable contains a valid software package.
            # Removes the specified computer from the chosen software package group.
            If ($SoftwarePackageList.Name -Like $SoftwarePackage)
            {
                # Begins to remove the computer from the software package's group.
                Write-Verbose "Computer exists, removing from $SoftwarePackage group..."
                $DSMComputer = Get-EmdbComputer $ComputerName -recurse
                $SoftPackage = Get-emdbGroup "$Server\Software\Software- One offs\$SoftwarePackage" -SchemaTag "Group"
                Remove-EmdbGroupMember -Group $SoftPackage -Member $DSMComputer
                Write-Verbose "Successfully removed $ComputerName from $SoftwarePackage group."
            }
            # Verifies that the SoftwarePackage is not equal to $Null or nothing. Must contain value
            # Will output a listing of packages that could be what you are looking for based on value in the $SoftwarePackage variable.
            ElseIf ($SoftwarePackage -ne $Null)
            {
                Write-Verbose "Software package does not exist, did you mean?" -Verbose
                Write-Output $SoftwarePackageList | Where-Object {$_.Name -like "*$SoftwarePackage*"}| Select -Expand Name
            }
            Else
            {
                Write-Verbose "Missing SoftwarePackage argument!" -Verbose
            }
        }
        Else
        {
            Write-Verbose "Computer $ComputerName does not exist, try creating it first before removing packages." -Verbose
        }	
	} # Prcoess
	End {} #End
}
# Reinstalls DSM computer object
Function Restore-DSMComputer
{
	<#
	.SYNOPSIS
        Will reinstall DSM computer object.
	.DESCRIPTION
        Issues a reinstall command for the computer object to the DSM server. Requires PXE boot of computer.
	.PARAMETER ComputerName

	.EXAMPLE
        Restore-DSMComputer WS-DE-5290
        Will issue a reinstall command for WS-DE-5290. PXE boot required.
	.NOTES 
	    Name: Restore-DSMComputer
	    Author: Scott Goyette
	    Date: 05/06/2016
	    Email: sgoyette@thebancorp.com, scottcgoyette@yahoo.com
	    Keywords:
	#>
	[CmdLetbinding(SupportsShouldProcess=$True)]
	param
	(
        [Parameter (Position=0, Mandatory=$True,
					ValueFromPipeline=$True,
					ValueFromPipelineByPropertyName=$True)]
		[String[]]$ComputerName
	)
	BEGIN
	{}#Begin
	PROCESS
	{
        ForEach ($Name in $ComputerName)
        {
            # Section moves into the correct PSDrive space based on hostname executing command
            $ExecutionHost = $env:COMPUTERNAME
            Write-Verbose "Executing command from $ExecutionHost..."
            Write-Verbose "Extracting Hardware and Site location from Computer Name..."
            # Splits the COmputer Name into 3 pieces: Hardware, Site, Extension
            Split-ComputerName $Name
            # Locates the correct OU to be in based on the server executing the command and the hardware type
            switch($ExecutionHost)
            {
                "PPPH-DEPLOY-1"
                {
                    Mount-DSM Philadelphia
                    Write-verbose "Philadelphia: Mounted accessing drive..."
                    switch($Computer.Hardware)
                    {
                        "LT" {cd "Philadelphia:\RootDSE\Managed Users & Computers\Laptops"; $Server ="Philadelphia:\RootDSE\Managed Users & Computers\Laptops"}
                        "TB" {cd "Philadelphia:\RootDSE\Managed Users & Computers\Laptops"; $Server ="Philadelphia:\RootDSE\Managed Users & Computers\Laptops"}
                        "WS" {cd "Philadelphia:\RootDSE\Managed Users & Computers\Workstations"; $Server ="Philadelphia:\RootDSE\Managed Users & Computers\Workstations"}
                    }
                }
                "PPSF-DEPLOY-2"
                {
                    Mount-DSM Sioux
                    Write-verbose "Sioux: Mounted accessing drive..."
                    switch($Computer.Hardware)
                    {
                        "LT" {cd "Sioux:\RootDSE\Managed Users & Computers\Laptops"; $Server ="Sioux:\RootDSE\Managed Users & Computers\Laptops"}
                        "TB" {cd "Sioux:\RootDSE\Managed Users & Computers\Laptops"; $Server ="Sioux:\RootDSE\Managed Users & Computers\Laptops"}
                        "WS" {cd "Sioux:\RootDSE\Managed Users & Computers\Workstations"; $Server ="Sioux:\RootDSE\Managed Users & Computers\Workstations"}
                    }    
                }
                "VPBG2-DSM-1"
                {
                    Mount-DSM Bulgaria
                    Write-verbose "Bulgaria: Mounted accessing drive..."
                    switch($Computer.Hardware)
                    {
                        "LT" {cd "Bulgaria:\RootDSE\Managed Users & Computers\Laptops"; $Server ="Bulgaria:\RootDSE\Managed Users & Computers\Laptops"}
                        "TB" {cd "Bulgaria:\RootDSE\Managed Users & Computers\Laptops"; $Server ="Bulgaria:\RootDSE\Managed Users & Computers\Laptops"}
                        "WS" {cd "Bulgaria:\RootDSE\Managed Users & Computers\Workstations"; $Server ="Bulgaria:\RootDSE\Managed Users & Computers\Workstations"}
                    }    
                }
                "VPWI-DSM-1"
                {
                    Mount-DSM Delaware
                    Write-verbose "Delaware: Mounted accessing drive..."
                    switch($Computer.Hardware)
                    {
                        "LT" {cd "Delaware:\RootDSE\Managed Users & Computers\Laptops"; $Server ="Delaware:\RootDSE\Managed Users & Computers\Laptops"}
                        "TB" {cd "Delaware:\RootDSE\Managed Users & Computers\Laptops"; $Server ="Delaware:\RootDSE\Managed Users & Computers\Laptops"}
                        "WS" {cd "Delaware:\RootDSE\Managed Users & Computers\Workstations"; $Server ="Delaware:\RootDSE\Managed Users & Computers\Workstations"}
                    }
                }
            }
            # Grabs the site location for our hardware
            Switch ($Computer.Location)
			{
				"BUL"	{ $Site = "Sofia" }
				"CH"	{ $Site = "Chicago" }
				"DE"	{ $Site = "Wilmington" }
				"FL"	{ $Site = "Orlando" }
				"GIB"	{ $Site = "Gibraltar" }
				"KE"	{ $Site = "Kent" }
				"KOP"	{ $Site = "KingOfPrussia" }
				"MD"	{ $Site = "Crofton" }
				"MN"	{ $Site = "Minneapolis" }
                "NY"    { $Site = "New York" }
				"PHL"	{ $Site = "Philadelphia" }
				"RU"	{ $Site = "Remote Sites"}
				"SF"	{ $Site = "Sioux Falls" }
                "TA"    { $Site = "Tampa" }
			}
            # Changes to the directory that computer name should be located.
            CD $Server\$Site   
            # If the computer name is a valid object in DSM issue a Reinstall
            If (Get-EmdbComputer "$Name" -Recurse)
            {
                Write-Verbose "$Name computer object is being set to reinstall immediately..."
                # This Cmdlet works this way because we already navigated to the drivespace where the computer object resides.
                Reinstall-EmdbComputer "$Name" -StartImmediately -RecalculateInstallationOrder
                Write-Verbose " $Name is ready for a PXE boot to begin reinstall!" -Verbose
            }
            ELse
            {
                Write-Verbose "$Name computer object does not exist within DSM, try creating the object first!" -Verbose
            }
        }
	    	
	}
	End { } #End
}
# Sets Windows Activation to KMS
Function Set-Activation
{
	<#
	.SYNOPSIS
		Sets Windows Activation to KMS
	.DESCRIPTION
	
	.PARAMETER ComputerName
	.EXAMPLE
	.NOTES 
		Name: Set-Activation
		Author: Scott Goyette
		Date: 12/01/2015
		Email: sgoyette@thebancorp.com, sgoyette@netwow.biz
		Keywords: Activation, Set-Activation, KMS
	#Requires -Version 4.0
	#>
	[CmdLetbinding()]
	param
	(
		[Parameter (Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$true)]
		[String[]]$ComputerName
	)
	BEGIN { } #Begin
	PROCESS
	{
		# Assigns OS version
		$OSversion = (Get-WmiObject -ComputerName $ComputerName -class Win32_OperatingSystem).Caption
		# COmpares OS version
		switch -Regex ($OSversion)
		{
			'Windows 7 Professional'                 { $Key = 'FJ82H-XT6CR-J8D7P-XQJJ2-GPDD4'; break }
			'Windows 7 Professional N'               { $Key = 'MRPKT-YTG23-K7D7T-X2JMM-QY7MG'; break }
			'Windows 7 Enterprise'                   { $Key = '33PXH-7Y6KF-2VJC9-XBBR8-HVTHH'; break }
			'Windows 7 Enterprise N'                 { $Key = 'YDRBP-3D83W-TY26F-D46B2-XCKRJ'; break }
			'Windows 8.1 Professional N'             { $key = 'HMCNV-VVBFX-7HMBH-CTY9B-B4FXY'; break }
			'Windows 8.1 Pro'                        { $key = 'GCRJD-8NW9H-F2CDX-CCM8D-9D6T9'; break }
			'Windows 8.1 Enterprise N'               { $key = 'TT4HM-HN7YT-62K67-RGRQJ-JFFXW'; break }
			'Windows 8.1 Enterprise'                 { $key = 'MHF9N-XY6XB-WVXMC-BTDCT-MKKG7'; break }
		}
		
		$KMSservice = Get-WMIObject -ComputerName $ComputerName -query "select * from SoftwareLicensingService"
		Write-Debug 'Activating Windows.'
		$null = $KMSservice.InstallProductKey($key)
		$null = $KMSservice.RefreshLicenseStatus()
	}
	End { } #End
}
# Sets the Asset Tag in the Dell BIOS
Function Set-AssetTag
{
	<#
	.SYNOPSIS
		Enters the Bancorp Asset Tag information field in the Dell BIOS utilizing the Dell Command Monitor.
		Making this process entirely hands off.
	.DESCRIPTION 
		Will set the Asset Tag field in the Dell BIOS utilizing the Dell Command Monitor Service (DCStor64). To fix
		a minor error within the DCStor64 service, we do a reboot of the service by stopping and then starting.
	.PARAMETER ComputerName
		Computer name of target machine to inject with Asset Tag.
	.PARAMETER AssetTag
		Asset Tag to be injected into Dell Bios.
	.EXAMPLE
		Set-AssetTag -ComputerName WS-DE-9999 -AssetTag A08888
		Will assign the Asset Tag A08888 to computer WS-DE-9999. Utilizing the Dell Command Monitor.
	.NOTES 
		Name: Set-AssetTag
		Author: Scott Goyette
		Date: 11/28/2015
		Email: sgoyette@thebancorp.com, sgoyette@netwow.biz
		Keywords: Asset Tag, AssetTag, Dell, Command Monitor
	#Requires -Version 4.0
	#>
	[CmdLetbinding(SupportsShouldProcess=$True)]
	param
	(
		[Parameter (Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$true)]
		[String[]]$ComputerName,
		[Parameter (Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$true)]
		[String[]]$AssetTag
	)
	BEGIN { } #Begin
	PROCESS
	{
		# Need to validate the Manufacturer type, Dell and Microsoft Surface Pro's have two entirely different ways to set the Asset Tag info.
		$Manufacturer = (Get-WmiObject -ComputerName $ComputerName -query "Select * from Win32_SystemEnclosure").Manufacturer
		switch ($Manufacturer)
		{
			"Dell Inc."
			{
				# This does the adding/ changing to BIOS to add Asset Tag, Service is first stopped and then Started again.
				# DCStor64 service has issues when you rapidly change the Asset Tag info, stopping then running before change fixes the issue. <--- Deployed to Dell types
				Get-Service -Name DCStor64 -ComputerName $ComputerName | Set-Service -Status stopped
				Get-Service -Name DCStor64 -ComputerName $ComputerName | Set-Service -Status Running
				(gwmi DCIM_Chassis -ComputerName $ComputerName -namespace root\dcim\sysman).ChangeAssetTag("$AssetTag")
			}
			
			"Microsoft Corporation"
			{
				# This assumes the AssetTag.exe was depolyed during setup, found within DesktopSupport on the root drive. <--- deployed to Microsoft Surface 3 and 4 types.
				# Also assumes person running has PSExec.exe in their DesktopSupport folder. <--- this should be changed to a resource location we all have access to.
				& C:\DesktopSupport\pstools\PsExec.exe -s   \\$ComputerName  "C:\DesktopSupport\AssetTag" `-s $AssetTag
			}
		}
		
		
	}
	End { } #End
}
# Splits $ComputerName into 3 parts
Function Split-ComputerName
{
	<#
	.SYNOPSIS
		Splits the $ComputerName variable up into 3 pieces
	.DESCRIPTION
		Splits $ComputerName into 3 variables: $Hardware, $Location, $PhoneExt. We now can utilize those pieces individually.
	.PARAMETER ComputerName
		Computer name to split up
	.EXAMPLE
		Split-ComputerName -ComputerName WS-DE-SPA08888
		Will split WS-DE-SPA08888 into 3 variables $Hardware = "WS", $Location = "DE", and $PhoneExt = "SPA08888"
	.INPUT
		ComputerName
	.OUTPUTS
		Global - Computer
		3 properties:
				Computer.Hardware - Whether Laptop, Tablet, or Workstation
				Computer.Location - Usually an office location
				Computer.PhoneExt - 4 digit number usually belonging to user
	.NOTES 
		Name: Split-ComputerName
		Author: Scott Goyette
		Date: 12/01/2015
		Email: sgoyette@thebancorp.com, sgoyette@netwow.biz
		Keywords: 
	#Requires -Version 4.0
	#>
	[CmdLetbinding()]
	param
	(
		[Parameter (Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$true)]
		[String[]]$ComputerName
	)
	BEGIN { } #Begin
	PROCESS
	{
		# Splits the $ComputerName variable into manageable chunks
		$Split = $ComputerName.split("-")
		$Global:Computer = New-Object -TypeName PSObject -Property @{
			"Hardware" = $Split[0]
			"Location" = $Split[1]
			"PhoneExt" = $Split[2]
		}
	}
	End { } #End
}
# Unlocks user account on all DC's
function Unlock-ADuser
{
	<#
	.SYNOPSIS
	    Unlocks users on all available Domain Controllers	
	.DESCRIPTION
		Checks username against Active Directory, then unlocks user on each available Domain Controller
	.PARAMETER Username
	    Username of user that is locked out of Active Directory
	.EXAMPLE
		Unlock-ADuser sftest
        Performs the Operation "Set" on user SF Test on all available Domain Controllers
    .NOTES 
		Name: Unlock-ADUser
		Author: Jeroen Ronk
		Date: 4/7/2016
		Email: jeronk@thebancorp.com
		Keywords: Unlock, AD, Activer Directory
	#>	
	[CmdLetbinding(SupportsShouldProcess=$True)]
	[OutputType([System.Object])]
	param
	(
		[Parameter (Mandatory=$true,
					ValueFromPipeline=$true,
					ValueFromPipelineByPropertyName=$true)]
		[String[]]$Username
	)
	BEGIN {Import-Module ActiveDirectory -force} #Begin
	PROCESS
	{
	    
        $DClist = Get-ADComputer -filter * -SearchBase 'ou=Domain Controllers,dc=thebancorp, dc=local'
        $DCexclude = @('VPBG2-DC-2', 'VPPH-DC-1', 'VPBG2-DC-1', 'CPAE-DC-1', 'CPAE-DC-2')
        $DCavail = Compare-Object $DClist.Name $DCexclude -PassThru
        $Filter = "sAmAccountname -eq ""$UserName"""
        if (Get-ADUser -Filter $Filter)
        {
    
            Foreach ($Unlock in $DCavail)
            {
                $User = Get-ADUser -Filter $Filter
                Write-Verbose "$Username is getting unnlocked now on $Unlock" -Verbose
                Unlock-ADAccount $User -Server $Unlock -ErrorAction SilentlyContinue -Verbose
            } #Foreach
        }
        Else 
        { 
            Write-Verbose "$Username not in Active Directory. Are you sure you didn't make a typo?" -Verbose
        } #ifelse	
	} # Process
	End {} #End
}