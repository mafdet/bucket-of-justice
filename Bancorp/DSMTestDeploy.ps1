﻿$ComputerName = "WS-DE-Test001"
$OS = Get-EmdbGroup "Delaware:\RootDSE\Managed Users & Computers\OS Deployment\Windows 7 (x64) Pro" -SchemaTag "Group"
$CoreApps = Get-EmdbGroup "Delaware:\RootDSE\Managed Users & Computers\Software\!Group- Core Applications" -SchemaTag "Group"
cd "Delaware:\RootDSE\Managed Users & Computers\Workstations\Wilmington"
$Computer = New-Item $ComputerName -ItemType "Computer" -DelayCreation
$Computer.InitialMACAddress = "5C260A4A01F6" 
$Computer.OperationMode = "Active"
$Computer.Create()
$Computer = Get-EmdbComputer "Delaware:\RootDSE\Managed Users & Computers\Laptops\Wilmington\$ComputerName"
Add-EmdbGroupMember -Group $OS -Member $Computer
Add-EmdbGroupMember -Group $CoreApps -Member $Computer